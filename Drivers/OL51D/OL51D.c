#include "OL51D.h"
#include "spi.h"
#include "gpio.h"
#include "string.h"
#include "stdlib.h"	
#include "BIT1628B.h"

#define uDISPLAY_SPI                 hspi1
#define BUFFER_SIZE                  32 						 	 
uint8_t uDBuf[BUFFER_SIZE]= {0x00};
uint8_t Mode0_Buf[10]= {0x00};
uint8_t Mode1_Buf[10]= {0x00};
uint8_t Mode2_Buf[10]= {0x00};
uint8_t Mode3_Buf[10]= {0x00};
uint8_t counter= 0;
uint8_t ON_Count= 1;
uint8_t OFF_Count= 1;

uint8_t Luminance_Mode= 0;

uint8_t Settings_1[42]= {
                          0x00  //0x03
                         ,0x00  //0x04	
                         ,0x00  //0x05	   
                         ,0x00  //0x06		
                          };
extern  uint8_t BITEK_RX_Buf[10];
extern  uint8_t STD_Format;												

#define uDISPLAY_XCLR_LOW()				  	HAL_GPIO_WritePin(XCLR_GPIO_Port, XCLR_Pin, GPIO_PIN_RESET)    // XCLR LOW
#define uDISPLAY_XCLR_HIGH()					HAL_GPIO_WritePin(XCLR_GPIO_Port, XCLR_Pin, GPIO_PIN_SET)      // XCLR HIGH

#define uDISPLAY_XCS_HIGH()   				HAL_GPIO_WritePin(XCS_GPIO_Port, XCS_Pin, GPIO_PIN_SET)       // XCS HIGH
#define uDISPLAY_XCS_LOW()  		  		HAL_GPIO_WritePin(XCS_GPIO_Port, XCS_Pin, GPIO_PIN_RESET)     // XCS LOW

#define uDISPLAY_E2_XCS_HIGH()   			HAL_GPIO_WritePin(E2_XCS_GPIO_Port, E2_XCS_Pin, GPIO_PIN_SET)    // E2_XCS HIGH
#define uDISPLAY_E2_XCS_LOW()   			HAL_GPIO_WritePin(E2_XCS_GPIO_Port, E2_XCS_Pin, GPIO_PIN_RESET)  // E2_XCS LOW

#define uDISPLAY_MODE_LSB()   			  HAL_GPIO_WritePin(MODE_GPIO_Port, MODE_Pin, GPIO_PIN_SET		   // HIGH: LSB First  
#define uDISPLAY_MODE_MSB()   			  HAL_GPIO_WritePin(MODE_GPIO_Port, MODE_Pin, GPIO_PIN_RESET)	 // LOW : MSB First

#define uDISPLAY_E2_WP_HIGH()   			HAL_GPIO_WritePin(E2_WP_GPIO_Port,E2_WP_Pin, GPIO_PIN_SET)      // E2PROM_WP:HIGH
#define uDISPLAY_E2_WP_LOW()   			  HAL_GPIO_WritePin(E2_WP_GPIO_Port,E2_WP_Pin, GPIO_PIN_RESET)    // E2PROM_WP:LOW

#define uDISPLAY_SO_SEL_HIGH()   			HAL_GPIO_WritePin(SO_SEL_GPIO_Port, SO_SEL_Pin , GPIO_PIN_SET)   // E2PROM SO ----> MCU SO
#define uDISPLAY_SO_SEL_LOW()   			HAL_GPIO_WritePin(SO_SEL_GPIO_Port, SO_SEL_Pin , GPIO_PIN_RESET) // OL51D SO  ----> MCU SO

void OL51D_Init(void)
{
   MX_SPI1_Init();
	 uDISPLAY_E2_WP_HIGH();      // Not Sure about it
	 uDISPLAY_MODE_MSB();        // MSB is first for both Read and Write (normal transfer)
	 OL51D_POWERON();
}
/*...........................................................................................................
                                    uDisplay Power OFF Sequence
...........................................................................................................*/
void OL51D_POWEROFF(void)
{

	 uDISPLAY_XCS_LOW();
	 uDisplay_SerialSettings(4);
	 uDISPLAY_XCS_HIGH();
	
	 HAL_Delay(40);               // Wait from 2~3 XVD =~ 40 msec
	 uDISPLAY_10V0_OFF();
	 HAL_Delay(5);
	 uDISPLAY_12V5_OFF();
	 HAL_Delay(5);   
   
	  /* Disable MCLK,VSYNC and HSYNC */
	 WriteRegister_8b(0x0FE,0x7F);         // Disable MCLK and RGB
	 WriteRegister_8b(0x0FB,0x0F);         // Disable HSYNC  	
	 WriteRegister_8b(0x0FC,0x0F);         // Disable VSYNC  	
	
	 HAL_Delay(30); 
	 uDISPLAY_XCLR_LOW();
   uDISPLAY_3V2_OFF();

}
/*...........................................................................................................
                                    uDisplay Power ON Sequence
...........................................................................................................*/
void OL51D_POWERON(void)
{
   uDISPLAY_XCLR_LOW();
	 uDISPLAY_3V2_ON();
	
	 HAL_Delay(10);
	
	 uDISPLAY_XCS_HIGH();
   uDISPLAY_E2_XCS_HIGH();	
	
	 HAL_Delay(5);
	
   uDISPLAY_E2_XCS_LOW();	    // Access to EEPROM 	
	 uDISPLAY_EEPROM_READ();    // Read All register settings from EEPROM
	 uDISPLAY_E2_XCS_HIGH();	
	
	 uDISPLAY_XCLR_HIGH();

	  /*	Enable MCLK,VSYNC and HSYNC  */ 
	 WriteRegister_8b(0x0FC,0x02);         // Enable VSYNC  	
	 WriteRegister_8b(0x0FB,0x00);         // Enable HSYNC 	
	 WriteRegister_8b(0x0FE,0x38);         // Enable MCLK and RGB
	 
	 uDisplay_SerialSettings(1);           //T1
	
	 uDISPLAY_12V5_ON();
	 HAL_Delay(4);
	 uDISPLAY_10V0_ON();
	 HAL_Delay(30);           
		
	 uDisplay_SerialSettings(2);           // T2  , T2-T1 need to be 2XVD (2*1/60)
	 uDisplay_SerialSettings(3);           // T3
	 HAL_Delay(35);                        // Wait at least 2XVD (2*1/60 =~33.333 msec) 
			
	 WriteRegister_8b(0x0FE,0x38);         // Enable MCLK
}
//=============================================================================================================
_Bool uDISPLAY_EEPROM_READ(void)
{
	 memset(Mode0_Buf, 0, sizeof(Mode0_Buf));
   EEPROM_SPI_ReadBuffer(Mode0_Buf,0x00,10);
	
			
	 memset(Mode1_Buf, 0, sizeof(Mode1_Buf));
   EEPROM_SPI_ReadBuffer(Mode1_Buf,0x20,10);
		
			
	 memset(Mode2_Buf, 0, sizeof(Mode2_Buf));
   EEPROM_SPI_ReadBuffer(Mode2_Buf,0x30,10);
		
			
	 memset(Mode3_Buf, 0, sizeof(Mode3_Buf));
   EEPROM_SPI_ReadBuffer(Mode3_Buf,0x40,10);	 
				
}
//=============================================================================================================
_Bool EEPROM_SPI_ReadBuffer(uint8_t* pBuffer, uint16_t ReadAddr, uint16_t NumByteToRead) 
{
    static uint8_t header[3];

    header[0] = EEPROM_READ;    // Send "Read from Memory" instruction
    header[1] = ReadAddr;       // Send 8-bit start address
	
	  uDISPLAY_SO_SEL_HIGH();     // SO Switch Select to E2PROM SO

    /* Send WriteAddr address byte to read from */
    if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)header,2, 200) != HAL_OK) 
			 {
         Error_Handler();
       }

    while(HAL_SPI_Receive(&uDISPLAY_SPI, (uint8_t*)pBuffer, NumByteToRead, 200) == HAL_BUSY) 
			  {
          HAL_Delay(1);
        };

	  uDISPLAY_SO_SEL_LOW();     // SO Switch Select to Panel SO

    return 1;
}
//=============================================================================================================
bool SPI_WriteRegister_8b(uint8_t reg_add,uint8_t value)
{
    uDBuf[0] = reg_add;    
    uDBuf[1] = value;  
	
	uDISPLAY_XCS_LOW();
	if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		{			
			Error_Handler();
		}
	uDISPLAY_XCS_HIGH();	
}
//=============================================================================================================
bool SPI_WriteRegister_Burst(uint8_t reg_add,uint8_t *array,uint8_t len)
{
    static uint8_t i=0;

    uDBuf[0] = reg_add;    
    for(i=1;i<len;i++)   
       {
			   uDBuf[i] = array[i-1];  
			 }  	
	if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf,len+1, 200) != HAL_OK) 
		{	
			Error_Handler();
		}
}
//=============================================================================================================	
uint8_t SPI_ReadRegister_8b_0(uint8_t reg_add)
{
	  uDISPLAY_SO_SEL_LOW();
		SPI_WriteRegister_8b(0x02,0x04);     // Set the RD_REGEN register(0x02h DATA2) to1
		SPI_WriteRegister_8b(0x80,0x01);     // Set the RD_ON register (0x80h DATA0) to 1
	  //============================
		
	  //SPI_WriteRegister_8b(0x81,reg_add);  // Write the address of reading data to 0x81h
    uDBuf[0] = 0x81;    
    uDBuf[1] = reg_add;  
	
	  uDISPLAY_XCS_LOW();
	
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {	
			   Error_Handler();
		   }
			 
	  uDISPLAY_XCS_HIGH();
		HAL_Delay(5);
		uDISPLAY_XCS_LOW();	

    uDBuf[0] = 0x81;    
    uDBuf[1] = 0x00;  			 
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 1, 200) != HAL_OK) 
		   {			
				Error_Handler();
		   }	
			 
    while(HAL_SPI_Receive(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) == HAL_BUSY) 
			  {	
          HAL_Delay(1);
        };	
		
    SPI_WriteRegister_8b(0x80,0x00);  				
		SPI_WriteRegister_8b(0x02,0x00);  		
		uDISPLAY_XCS_HIGH();
		//===========================
		return uDBuf[0];
	
}

//=============================================================================================================	
uint8_t SPI_ReadRegister_8b(uint8_t reg_add)
{
	  uDISPLAY_SO_SEL_LOW();
		SPI_WriteRegister_8b(0x02,0x04);     // Set the RD_REGEN register(0x02h DATA2) to1
		SPI_WriteRegister_8b(0x80,0x01);     // Set the RD_ON register (0x80h DATA0) to 1
	  //============================
		
	  //SPI_WriteRegister_8b(0x81,reg_add);  // Write the address of reading data to 0x81h
    uDBuf[0] = 0x81;    
    uDBuf[1] = reg_add;  
	
	  uDISPLAY_XCS_LOW();
	
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {	
			   Error_Handler();
		   }
			 
	  uDISPLAY_XCS_HIGH();
		HAL_Delay(5);
		uDISPLAY_XCS_LOW();	

    uDBuf[0] = 0x81;    
    uDBuf[1] = 0x00;  			 
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {		
				Error_Handler();
		   }	
			 
    while(HAL_SPI_Receive(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) == HAL_BUSY) 
			  {	
          HAL_Delay(1);
        };	

    SPI_WriteRegister_8b(0x80,0x00);  				
		SPI_WriteRegister_8b(0x02,0x00);  				
		uDISPLAY_XCS_HIGH();
		//===========================
		return uDBuf[1];
	
}

//==============================================================================
uint8_t SPI_ReadRegister_8b_2(uint8_t reg_add)
{
	  uDISPLAY_SO_SEL_LOW();
		SPI_WriteRegister_8b(0x02,0x04);     // Set the RD_REGEN register(0x02h DATA2) to1
		SPI_WriteRegister_8b(0x80,0x01);     // Set the RD_ON register (0x80h DATA0) to 1
	  //============================
		
	  //SPI_WriteRegister_8b(0x81,reg_add);  // Write the address of reading data to 0x81h
    uDBuf[0] = 0x81;    
    uDBuf[1] = reg_add;  
	
	  uDISPLAY_XCS_LOW();
	
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {
			   Error_Handler();
		   } 
	  uDISPLAY_XCS_HIGH();
			 
		HAL_Delay(5);
			 
		uDISPLAY_XCS_LOW();	

    uDBuf[0] = 0x81;    
    uDBuf[1] = 0x00;  			 
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {			
				Error_Handler();
		   }	
			 
    while(HAL_SPI_Receive(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) == HAL_BUSY) 
			  {	
          HAL_Delay(1);
        };	

    SPI_WriteRegister_8b(0x80,0x00);  				
		SPI_WriteRegister_8b(0x02,0x00);  				
		uDISPLAY_XCS_HIGH();
		//===========================
		return uDBuf[1];
	
}
//=============================================================================================================
//==============================================================================
uint8_t SPI_ReadRegister_8b_3(uint8_t reg_add)
{
	  uDISPLAY_SO_SEL_LOW();
		SPI_WriteRegister_8b(0x02,0x04);     // Set the RD_REGEN register(0x02h DATA2) to1
		SPI_WriteRegister_8b(0x80,0x01);     // Set the RD_ON register (0x80h DATA0) to 1
	  //============================
		
	  //SPI_WriteRegister_8b(0x81,reg_add);  // Write the address of reading data to 0x81h
    uDBuf[0] = 0x81;    
    uDBuf[1] = reg_add;  
	
	  uDISPLAY_XCS_LOW();
	
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) != HAL_OK) 
		   {
			   Error_Handler();
		   } 
	  uDISPLAY_XCS_HIGH();
			 
		HAL_Delay(5);
			 
		uDISPLAY_XCS_LOW();	

    uDBuf[0] = 0x81;    
    uDBuf[1] = 0x00;  			 
	  if(HAL_SPI_Transmit(&uDISPLAY_SPI, (uint8_t*)uDBuf, 1, 200) != HAL_OK) 
		   {			
				Error_Handler();
		   }	
			 
    while(HAL_SPI_Receive(&uDISPLAY_SPI, (uint8_t*)uDBuf, 2, 200) == HAL_BUSY) 
			  {	
          HAL_Delay(1);
        };	

    SPI_WriteRegister_8b(0x80,0x00);  				
		SPI_WriteRegister_8b(0x02,0x00);  				
		uDISPLAY_XCS_HIGH();
		//===========================
		return uDBuf[1];
	
}
//=============================================================================================================
void uDisplay_SerialSettings(uint8_t order)
{
	  static uint8_t temp=0;
	
   	switch(order)
	     {
				 case 1:
					      temp=Mode0_Buf[0] & 0xC0;
					      if(STD_Format == 0x50)
								  {
									 temp=(temp | 0x0A ) ;               // FORMAT=XGA-50Hz
									}
								else
								  {
									 STD_Format=0x60;									
									 temp=(temp | 0x00 ) ;               // FORMAT=XGA-59.94Hz
								  }
								
					      SPI_WriteRegister_8b(0x00,0x0E);      // Power saving ON , MCLKPOL=NEG , Left scan ,  Upper scan , HD_POL=NEG , VD_POL=NEG 				 
       				  SPI_WriteRegister_8b(0x01,temp);      // Dithering Off ,GAMSL as per chosen profile
				        SPI_WriteRegister_8b(0x02,0x32);      // GBRTEN=1 , GCONEN=1 ,Timing setting=Invalid (as input),Read disabled
                SPI_WriteRegister_8b(0x03,0x00);      // AMPBIAS=00 ,TMPEN=00 (Temperature compensation On)
      				 //SPI_WriteRegister_Burst(0x03,Settings_1,42);
				        //==============================
				        temp=(Mode0_Buf[0] & 0x1F ) ;
				        SPI_WriteRegister_8b(0x04,temp);      // CALSEL=00 ,T_SLOPE=0 ,CALDAC as per chosen profile
				        SPI_WriteRegister_8b(0x05,Mode0_Buf[1]);
				        SPI_WriteRegister_8b(0x06,Mode0_Buf[2]);
				        SPI_WriteRegister_8b(0x07,Mode0_Buf[3]);
				        SPI_WriteRegister_8b(0x08,Mode0_Buf[4]);	
				        SPI_WriteRegister_8b(0x09,Mode0_Buf[5]);
				        SPI_WriteRegister_8b(0x0A,Mode0_Buf[6]);
				        SPI_WriteRegister_8b(0x0B,Mode0_Buf[7]);
				        SPI_WriteRegister_8b(0x0C,Mode0_Buf[8]);					 
				        //==============================
				        SPI_WriteRegister_8b(0x34,0x06);
				        SPI_WriteRegister_8b(0x00,0x0F);	//Power saving OFF						 
					      break;
				 case 2:
					      SPI_WriteRegister_8b(0x34,0x00);
					      break;
				 case 3:
					      SPI_WriteRegister_8b(0x02,0x00);
					      break;
				 case 4:
					      SPI_WriteRegister_8b(0x00,0x0E);  //Power saving ON
					      break;				 
				default:
					      break;	
			}				
}


void uDisplay_SerialSettings_2(uint8_t order)
{
	  static uint8_t temp=0;
	
   	switch(order)
	     {
				 case 1:
					      SPI_WriteRegister_8b(0x00,0x00);  //Power saving ON , MCLKPOL=NEG , Left scan ,  Upper scan , HD_POL=NEG , VD_POL=NEG 
				        SPI_WriteRegister_8b(0x01,0xF0);  //FORMAT=XGA-59.94Hz ,Dithering On ,Gamma table Not Used
				        SPI_WriteRegister_8b(0x02,0x02);  //GBRTEN=Invalid , GCONEN=Invalid ,Timing setting=Invalid (as input),Read disabled
                //SPI_WriteRegister_Burst(0x03,Settings_1,42);
				        //==============================
				        temp=(Mode0_Buf[0] & 0x1F ) | ((Mode0_Buf[0] >>1) & 0x60);
				        SPI_WriteRegister_8b(0x04,temp);
				        SPI_WriteRegister_8b(0x05,Mode0_Buf[1]);
				        SPI_WriteRegister_8b(0x06,Mode0_Buf[2]);
				        SPI_WriteRegister_8b(0x07,Mode0_Buf[3]);
				        SPI_WriteRegister_8b(0x08,Mode0_Buf[4]);	
				        SPI_WriteRegister_8b(0x09,Mode0_Buf[5]);
				        SPI_WriteRegister_8b(0x0A,Mode0_Buf[6]);
				        SPI_WriteRegister_8b(0x0B,Mode0_Buf[7]);
				        SPI_WriteRegister_8b(0x0C,Mode0_Buf[8]);					 
				        //==============================
				        SPI_WriteRegister_8b(0x34,0x06);
				        SPI_WriteRegister_8b(0x00,0x0F);	//Power saving OFF						 
					      break;
				 case 2:
					      SPI_WriteRegister_8b(0x34,0x00);
					      break;
				 case 3:
					      SPI_WriteRegister_8b(0x02,0x00);
					      break;
				 case 4:
					      SPI_WriteRegister_8b(0x00,0x0E);  //Power saving ON
					      break;				 
				default:
					      break;	
			}				
}
//==============================================================================
void uDisplay_LuminanceMode(uint8_t mode)
{
	
	  static uint8_t Dtemp=0;
		
   	switch(mode)
	     {
				 case LUM_LOW:		
								Dtemp=(0x80 & 0xC0 ) ;

					      if(STD_Format == 0x50)
									Dtemp=(Dtemp | 0x0A ) ;               // FORMAT=XGA-50Hz
								else
									Dtemp=(Dtemp | 0x00 ) ;               // FORMAT=XGA-59.94Hz
								
								SPI_WriteRegister_8b(0x01,Dtemp);      

								Dtemp=(0x80 & 0x1F ) ;
							
								SPI_WriteRegister_8b(0x04,Dtemp);      
								SPI_WriteRegister_8b(0x05,Mode0_Buf[1]);
								SPI_WriteRegister_8b(0x06,Mode0_Buf[2]);
								SPI_WriteRegister_8b(0x07,Mode0_Buf[3]);
								SPI_WriteRegister_8b(0x08,Mode0_Buf[4]);	
								SPI_WriteRegister_8b(0x09,Mode0_Buf[5]);
								SPI_WriteRegister_8b(0x0A,Mode0_Buf[6]);
								SPI_WriteRegister_8b(0x0B,Mode0_Buf[7]);
								SPI_WriteRegister_8b(0x0C,Mode0_Buf[8]);	
					      break;	
				 case LUM_MED:		
								Dtemp=(0x88 & 0xC0 ) ;

					      if(STD_Format == 0x50)
									Dtemp=(Dtemp | 0x0A ) ;               // FORMAT=XGA-50Hz
								else
									Dtemp=(Dtemp | 0x00 ) ;               // FORMAT=XGA-59.94Hz
								
								SPI_WriteRegister_8b(0x01,Dtemp);      

								Dtemp=(0x88 & 0x1F ) ;
							
								SPI_WriteRegister_8b(0x04,Dtemp);      
								SPI_WriteRegister_8b(0x05,Mode0_Buf[1]);
								SPI_WriteRegister_8b(0x06,Mode0_Buf[2]);
								SPI_WriteRegister_8b(0x07,Mode0_Buf[3]);
								SPI_WriteRegister_8b(0x08,Mode0_Buf[4]);	
								SPI_WriteRegister_8b(0x09,Mode0_Buf[5]);
								SPI_WriteRegister_8b(0x0A,Mode0_Buf[6]);
								SPI_WriteRegister_8b(0x0B,Mode0_Buf[7]);
								SPI_WriteRegister_8b(0x0C,Mode0_Buf[8]);			
					      break;
				 case LUM_HIGH:		
								Dtemp=(0x8F & 0xC0 ) ;

					      if(STD_Format == 0x50)
									Dtemp=(Dtemp | 0x0A ) ;               // FORMAT=XGA-50Hz
								else
									Dtemp=(Dtemp | 0x00 ) ;               // FORMAT=XGA-59.94Hz
								
								SPI_WriteRegister_8b(0x01,Dtemp);      

								Dtemp=(0x8F & 0x1F ) ;
							
								SPI_WriteRegister_8b(0x04,Dtemp);      
								SPI_WriteRegister_8b(0x05,Mode0_Buf[1]);
								SPI_WriteRegister_8b(0x06,Mode0_Buf[2]);
								SPI_WriteRegister_8b(0x07,Mode0_Buf[3]);
								SPI_WriteRegister_8b(0x08,Mode0_Buf[4]);	
								SPI_WriteRegister_8b(0x09,Mode0_Buf[5]);
								SPI_WriteRegister_8b(0x0A,Mode0_Buf[6]);
								SPI_WriteRegister_8b(0x0B,Mode0_Buf[7]);
								SPI_WriteRegister_8b(0x0C,Mode0_Buf[8]);				 
				default:
					      break;	
			}		
}	

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	     HAL_NVIC_DisableIRQ(EXTI4_15_IRQn);
	
	     Luminance_Mode++;
       if(Luminance_Mode ==3)
            Luminance_Mode=0;
	
			 uDisplay_LuminanceMode(Luminance_Mode);	
				
       HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);				
}
//================================================================
void Input_Video_Check(void)
{	
	  OL51D_POWEROFF();
	  HAL_Delay(100);
	  OL51D_POWERON();					
}
