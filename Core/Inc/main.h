/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#define uDISPLAY_3V2_ON()       HAL_GPIO_WritePin(ON_3V2_GPIO_Port, ON_3V2_Pin, GPIO_PIN_SET)
#define uDISPLAY_3V2_OFF()      HAL_GPIO_WritePin(ON_3V2_GPIO_Port, ON_3V2_Pin, GPIO_PIN_RESET)

#define uDISPLAY_10V0_ON()      HAL_GPIO_WritePin(ON_10V_GPIO_Port, ON_10V_Pin, GPIO_PIN_SET)
#define uDISPLAY_10V0_OFF()     HAL_GPIO_WritePin(ON_10V_GPIO_Port, ON_10V_Pin, GPIO_PIN_RESET)

#define uDISPLAY_12V5_ON()      HAL_GPIO_WritePin(ON_12V5_GPIO_Port, ON_12V5_Pin, GPIO_PIN_SET)
#define uDISPLAY_12V5_OFF()     HAL_GPIO_WritePin(ON_12V5_GPIO_Port, ON_12V5_Pin, GPIO_PIN_RESET)
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
extern void I2C_Slave_Init(void);
extern void I2C_Slave_DeInit(void);
extern void I2C_Error_Handler(void);
extern void I2C_Set_Register(uint8_t reg, uint8_t data);
extern uint8_t I2C_Get_Register(uint8_t reg);
void vprint(const char *fmt, va_list argp);
void my_printf(const char *fmt, ...);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define XCLR_Pin GPIO_PIN_14
#define XCLR_GPIO_Port GPIOC
#define ON_3V2_Pin GPIO_PIN_15
#define ON_3V2_GPIO_Port GPIOC
#define MODE_Pin GPIO_PIN_0
#define MODE_GPIO_Port GPIOA
#define E2_XCS_Pin GPIO_PIN_1
#define E2_XCS_GPIO_Port GPIOA
#define B_TXD_Pin GPIO_PIN_2
#define B_TXD_GPIO_Port GPIOA
#define B_RXD_Pin GPIO_PIN_3
#define B_RXD_GPIO_Port GPIOA
#define SO_SEL_Pin GPIO_PIN_4
#define SO_SEL_GPIO_Port GPIOA
#define XSCK_Pin GPIO_PIN_5
#define XSCK_GPIO_Port GPIOA
#define SO_Pin GPIO_PIN_6
#define SO_GPIO_Port GPIOA
#define SI_Pin GPIO_PIN_7
#define SI_GPIO_Port GPIOA
#define XCS_Pin GPIO_PIN_0
#define XCS_GPIO_Port GPIOB
#define E2_WP_Pin GPIO_PIN_1
#define E2_WP_GPIO_Port GPIOB
#define BIT_RST_Pin GPIO_PIN_8
#define BIT_RST_GPIO_Port GPIOA
#define BRIGHT_Pin GPIO_PIN_9
#define BRIGHT_GPIO_Port GPIOA
#define BRIGHT_EXTI_IRQn EXTI4_15_IRQn
#define SWDIO_Pin GPIO_PIN_13
#define SWDIO_GPIO_Port GPIOA
#define SWCLK_Pin GPIO_PIN_14
#define SWCLK_GPIO_Port GPIOA
#define ON_12V5_Pin GPIO_PIN_15
#define ON_12V5_GPIO_Port GPIOA
#define ON_10V_Pin GPIO_PIN_3
#define ON_10V_GPIO_Port GPIOB
#define SCL_Pin GPIO_PIN_6
#define SCL_GPIO_Port GPIOB
#define SDA_Pin GPIO_PIN_7
#define SDA_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
