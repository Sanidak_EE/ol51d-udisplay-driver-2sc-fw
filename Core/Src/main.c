/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "BIT1628B.h"
#include "OSD.h"
#include "OL51D.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
   uint8_t SLAVES[10]={0};
	 uint8_t cnt=0;
	 extern struct ProcessingUnit_t ProcessingUnit;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
enum State { stop, waiting, getRegisterAddress, getData, sendData };

typedef struct i2c_slave_init_t i2c_slave_init_t;

struct i2c_slave_init_t{
  I2C_HandleTypeDef *hi2c;
  void (*error_handler)();
};

void (*_error_handler)();

struct i2c_slave_data_t{
  enum State transferState;
  uint8_t registerAddress;
  uint8_t receiveBuffer;
  uint8_t i2c_memory[0xFF];
}i2c_data;

void vprint(const char *fmt, va_list argp) 
	{
	char string[200];
	if (0 < vsprintf(string, fmt, argp)) // build string
			{
		    HAL_UART_Transmit(&huart2, (uint8_t*) string, strlen(string), 0xffffff); // send message via UART
	    }
  }

void my_printf(const char *fmt, ...) // custom printf() function
{
	va_list argp;
	va_start(argp, fmt);
	vprint(fmt, argp);
	va_end(argp);
}
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern  uint8_t STD_Format;
extern  uint8_t Luminance_Mode;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c);
void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t direction, uint16_t addrMatchCode);
void I2C_Slave_Init(void);
void I2C_Slave_DeInit(void);
void I2C_Error_Handler(void);
void I2C_Set_Register(uint8_t reg, uint8_t data);
uint8_t I2C_Get_Register(uint8_t reg);
void SCAN_Push_Buttons(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
i2c_slave_init_t I2C_init = {.hi2c = &hi2c1, .error_handler = Error_Handler};
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
	my_printf(" ########## OL51D uDisplay Driver Board 2SC FW ######## \r\n"); 
	HAL_Delay(100);	
  BITEK_version();				
	BITEK_Init1024();
  OL51D_Init();
	my_printf(" OL51D Initialization Done ! \r\n"); 		

  //Background and Test Pattern Values
	WriteRegister_8b(BITEK_03F_R_ATTR, 0x00);
	WriteRegister_8b(BITEK_040_G_ATTR, 0x00);
	WriteRegister_8b(BITEK_041_B_ATTR, 0x3F);		
	WriteRegister_8b(BITEK_045_HLCK_SEL, 0x08);	
	I2C_Slave_Init();

	 
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLLMUL_4;
  RCC_OscInitStruct.PLL.PLLDIV = RCC_PLLDIV_2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void I2C_Slave_Init(void)
{
  HAL_I2C_EnableListen_IT(I2C_init.hi2c);
  i2c_data.transferState = waiting;
  _error_handler = I2C_init.error_handler;
}

void I2C_Slave_DeInit(void)
{
	HAL_I2C_DisableListen_IT(I2C_init.hi2c);
  i2c_data.transferState = stop;
  _error_handler = I2C_init.error_handler;
}

void I2C_Error_Handler(){
  _error_handler();
}

void I2C_Set_Register(uint8_t reg, uint8_t data)
{
  i2c_data.i2c_memory[reg] = data;
}

uint8_t I2C_Get_Register(uint8_t reg)
{
  return i2c_data.i2c_memory[reg];
}

void HAL_I2C_AddrCallback(I2C_HandleTypeDef *hi2c, uint8_t direction, uint16_t addrMatchCode) {
	switch (direction) {
		case I2C_DIRECTION_TRANSMIT:
			i2c_data.transferState = getRegisterAddress;
			if (HAL_I2C_Slave_Seq_Receive_IT(hi2c, &i2c_data.receiveBuffer, 1, I2C_FIRST_FRAME) != HAL_OK) {
				I2C_Error_Handler();
			}
		break;
		case I2C_DIRECTION_RECEIVE:
			i2c_data.transferState = sendData;
			if (HAL_I2C_Slave_Seq_Transmit_IT(hi2c, &i2c_data.i2c_memory[i2c_data.registerAddress++], 255, I2C_NEXT_FRAME) != HAL_OK) {
				I2C_Error_Handler();
			}
		break;
    default:
      I2C_Error_Handler();
    break;
	}
}

void HAL_I2C_SlaveRxCpltCallback(I2C_HandleTypeDef *hi2c) {
	switch (i2c_data.transferState) {
		case getRegisterAddress:
			i2c_data.registerAddress = i2c_data.receiveBuffer;
			i2c_data.transferState = getData;
			if (HAL_I2C_Slave_Seq_Receive_IT(hi2c,&i2c_data.i2c_memory[i2c_data.registerAddress], 1, I2C_FIRST_FRAME) != HAL_OK) {
				I2C_Error_Handler();
			}
		break;
		case getData:
			if (HAL_I2C_Slave_Seq_Receive_IT(hi2c, &i2c_data.i2c_memory[i2c_data.registerAddress], 255, I2C_FIRST_FRAME) != HAL_OK) {
				I2C_Error_Handler();
			}
		Luminance_Mode=i2c_data.i2c_memory[i2c_data.registerAddress];
		uDisplay_LuminanceMode(Luminance_Mode); 
		break;
    default:
      I2C_Error_Handler();
    break;
	}
}

void HAL_I2C_ListenCpltCallback(I2C_HandleTypeDef *hi2c) {
	HAL_I2C_EnableListen_IT(hi2c);
}

void HAL_I2C_ErrorCallback(I2C_HandleTypeDef *hi2c) {
	if (HAL_I2C_GetError(hi2c) != HAL_I2C_ERROR_AF) {
		I2C_Error_Handler();
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
